FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

ARG RAILS_ROOT=/app/code

ENV RAILS_ENV=production
ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"
ENV GREENLIGHTVERSION=2.11.1

RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

# Make /root a symlink and writeable
RUN rm -rf /root/ && ln -s /app/data/root /root

# Install needed packages and install bundler
RUN apt-get update && apt-get install -y libsqlite3-dev tzdata  && rm -rf /var/cache/apt /var/lib/apt/lists
RUN gem install bundler

RUN wget https://github.com/bigbluebutton/greenlight/archive/release-${GREENLIGHTVERSION}.tar.gz \
     && tar xfv release-${GREENLIGHTVERSION}.tar.gz -C /app/code/ \
     && mv /app/code/greenlight-release-${GREENLIGHTVERSION}/* /app/code/ \
     && rm -r /app/code/greenlight-release-${GREENLIGHTVERSION}/ \
     && rm release-${GREENLIGHTVERSION}.tar.gz

RUN bundle install --deployment --without development:test:assets -j4 --path=vendor/bundle \
    && rm -rf vendor/bundle/ruby/2.7.0/cache/*.gem \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.c" -delete \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.o" -delete

COPY sample.env /app/code/

RUN rm -rf /app/code/tmp /app/code/log \
    && ln -s /tmp/greenlight/tmp /app/code/tmp \
    && ln -s /tmp/greenlight/tmp/log /app/code/log \
    && ln -s /app/data/yarn-error.log /app/code/yarn-error.log \
    && mv /app/code/yarn.lock /app/code/yarn.lock_stale \
    && mv /app/code/public /app/code/public_stale \
    && mv /app/code/app/views/main/ /app/code/app/views/main_stale \
    && mv /app/code/config/locales/ /app/code/config/locales_stale \
    && ln -s /app/data/locales /app/code/config/locales \
    && ln -s /app/data/views/ /app/code/app/views/main \
    && ln -s /app/data/public/ /app/code/public \
    && ln -s /app/data/yarn.lock /app/code/yarn.lock \
    && ln -s /app/data/.yarnrc /usr/local/share/.yarnrc \
    && ln -s /app/data/node_modules /app/code/node_modules \
    && ln -s /app/data/storage /app/code/storage \
    && echo 'export $(grep -v '^#' /app/data/.env | xargs)' >> /etc/bash.bashrc

COPY start.sh /app/code/bin/start

# Start the application.
CMD ["bin/start"]
