#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    var TEST_TIMEOUT = 20000;
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var LOCATION = 'test';

    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var adminEmail = 'admin@server.local';
    var adminPassword = 'changeme';

    let browser, app;

    before(function (done) {
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));

        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
        done();
    });

    after(function () {
        browser.quit();
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function ldap_login(username, password) {
        await browser.get(`https://${app.fqdn}/ldap_signin`);
        await waitForElement(By.id('session_username'));
        await browser.findElement(By.id('session_username')).sendKeys(username);
        await browser.findElement(By.id('session_password')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@value="Sign in"]')).click();
        await waitForElement(By.xpath('//h1[text()="Home Room"]'));
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/signin`);
        await waitForElement(By.id('session_email'));
        await browser.findElement(By.id('session_email')).sendKeys(username);
        await browser.findElement(By.id('session_password')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[@value="Sign in"]')).click();
        await waitForElement(By.xpath('//h1[text()="Home Room"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//span[contains(@class, "username")]'));
        await browser.findElement(By.xpath('//span[contains(@class, "username")]')).click();
        await waitForElement(By.xpath('//form[@action="/u/logout"]'));
        await browser.findElement(By.xpath('//form[@action="/u/logout"]')).click();
        await waitForElement(By.xpath('//a[text()="Sign in"]'));
    }

    async function createRoom(roomName='CloudronTestRoom') {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//h4[text()="Home Room"]'));
        await browser.findElement(By.xpath('//h4[text()="Create a Room"]')).click();
        await sleep(500);
        await browser.findElement(By.xpath('//input[@id="create-room-name"]')).sendKeys(roomName);
        await sleep(500);
        await browser.findElement(By.xpath('//input[@data-disable-with="Create Room"]')).click();
        await sleep(500);
        await waitForElement(By.xpath(`//h4[text()="${roomName}"]`));
    }

    async function checkForRoom(defaultRoom='CloudronTestRoom') {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath(`//h4[text()="${defaultRoom}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can create room', createRoom);
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);
    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can find room', checkForRoom);
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can find room', checkForRoom);
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);

    it('move to different location', function (done) {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
            done();
        });
    });
    it('can get app information', getAppInfo);

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can find room', checkForRoom);
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // // test update
     it('can install app', function () { execSync(`cloudron install --appstore-id org.bigbluebutton.greenlight.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
     it('can get app information', getAppInfo);

     it('can ldap_login', ldap_login.bind(null, username, password));
     it('can create room', createRoom);
     it('can logout', logout);
     it('can login', login.bind(null, adminEmail, adminPassword));
     it('can logout', logout);

     it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

     it('can get app information', getAppInfo);

     it('can ldap_login', ldap_login.bind(null, username, password));
     it('can find room', checkForRoom);
     it('can logout', logout);
     it('can login', login.bind(null, adminEmail, adminPassword));
     it('can logout', logout);

     it('uninstall app', function (done) {
         // ensure we don't hit NXDOMAIN in the mean time
         browser.get('about:blank').then(function () {
             execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
             done();
         });
     });
});
