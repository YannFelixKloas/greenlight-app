#!/bin/bash
set -eu

mkdir -p /tmp/greenlight/tmp

# GzEvD copy sample.env to /app/data/.env if not exsists
if [[ ! -f /app/data/.env ]]; then
    cp -v /app/code/sample.env /app/data/.env
fi

# GzEvD create needed folders for symlinks
if [[ ! -d /tmp/greenlight/tmp/log ]]; then
    mkdir -p /tmp/greenlight/tmp/log
fi

if [[ ! -f /app/data/yarn.lock ]]; then
  cp -v /app/code/yarn.lock_stale /app/data/yarn.lock
fi

if [[ ! -d /app/data/node_modules ]]; then
    mkdir -p /app/data/node_modules
fi

if [[ ! -d /app/data/views ]]; then
    cp -rv /app/code/app/views/main_stale /app/data/views
fi

if [[ ! -d /app/data/locales ]]; then
    cp -rv /app/code/config/locales_stale /app/data/locales
fi

if [[ ! -d /app/data/public ]]; then
    cp -rv /app/code/public_stale /app/data/public
fi

if [[ ! -d /app/data/root ]]; then
    mkdir -p /app/data/root
fi

if [[ ! -d /app/data/storage ]]; then
    mkdir -p /app/data/storage
fi
# END GzEvD

touch /app/data/yarn-error.log
touch /app/data/yarn.lock
touch /app/data/.yarnrc

export SECRET_KEY_BASE="$(bundle exec rake secret)"
export RAILS_ENV=production

sed -e "s/SECRET_KEY_BASE=.*/SECRET_KEY_BASE=$SECRET_KEY_BASE/g" \
    -e "s/DB_HOST=.*/DB_HOST=$CLOUDRON_POSTGRESQL_HOST/g" \
    -e "s/DB_PORT=.*/DB_PORT=$CLOUDRON_POSTGRESQL_PORT/g" \
    -e "s/DB_NAME=.*/DB_NAME=$CLOUDRON_POSTGRESQL_DATABASE/g" \
    -e "s/DB_USERNAME=.*/DB_USERNAME=$CLOUDRON_POSTGRESQL_USERNAME/g" \
    -e "s/DB_PASSWORD=.*/DB_PASSWORD=$CLOUDRON_POSTGRESQL_PASSWORD/g" \
    -i /app/data/.env

sed -i -e "s/LDAP_SERVER=.*/LDAP_SERVER=$CLOUDRON_LDAP_SERVER/g" \
    -e "s/LDAP_PORT=.*/LDAP_PORT=$CLOUDRON_LDAP_PORT/g" \
    -e "s/LDAP_BASE=.*/LDAP_BASE=$CLOUDRON_LDAP_USERS_BASE_DN/g" \
    -e "s/LDAP_BIND_DN=.*/LDAP_BIND_DN=$CLOUDRON_LDAP_BIND_DN/g" \
    -e "s/LDAP_PASSWORD=.*/LDAP_PASSWORD=$CLOUDRON_LDAP_BIND_PASSWORD=/g" \
    -e "s/LDAP_METHOD=.*/LDAP_METHOD=plain/g" \
    -e "s/LDAP_UID=.*/LDAP_UID=username/g" \
    -e "s/LDAP_AUTH=.*/LDAP_AUTH=simple/g" \
    -e "s/LDAP_ROLE_FIELD=.*/LDAP_ROLE_FIELD=users/g" \
    -e "s/LDAP_FILTER=.*/LDAP_FILTER=/g" \
    -e "s/LDAP_ATTRIBUTE_MAPPING=.*/LDAP_ATTRIBUTE_MAPPING=uid=uid;name=displayname;email=mail;nickname=givenName;/g" \
    -i /app/data/.env

sed -i -e "s/SMTP_SERVER=.*/SMTP_SERVER=$CLOUDRON_MAIL_SMTP_SERVER/g" \
    -e "s/SMTP_PORT=.*/SMTP_PORT=$CLOUDRON_MAIL_SMTP_PORT/g" \
    -e "s/SMTP_DOMAIN=.*/SMTP_DOMAIN=$CLOUDRON_MAIL_DOMAIN/g" \
    -e "s/SMTP_USERNAME=.*/SMTP_USERNAME=$CLOUDRON_MAIL_SMTP_USERNAME/g" \
    -e "s/SMTP_PASSWORD=.*/SMTP_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD/g" \
    -e "s/SMTP_AUTH=.*/SMTP_AUTH=login/g" \
    -e "s/SMTP_STARTTLS_AUTO=.*/SMTP_STARTTLS_AUTO=true/g" \
    -e "s/SMTP_SENDER=.*/SMTP_SENDER=$CLOUDRON_MAIL_FROM/g" \
    -i /app/data/.env

source /app/data/.env

# Export all varaibles in /app/data/.env
echo 'Reading /app/data/.env'
export $(grep -v '^#' /app/data/.env | xargs)

# Create default admin user if the file .default-admin-created does not exsist
if [[ ! -f /app/data/.default-admin-created ]]; then
    echo ">>> Detected first run"
    # bundle exec rake db:create # Not needed since cloudron creates database
    echo ">>> Loading schema"
    bundle exec rake db:schema:load

    echo ">>> Creating admin user"
    bundle exec rake user:create["admin","admin@server.local","changeme","admin"]
    touch /app/data/.default-admin-created
else
    echo ">>> Database migration"
    bundle exec rake db:migrate
fi

echo ">>> Compiling assets"
bundle exec --verbose rake assets:precompile --trace --verbose

echo ">>> Starting Greenlight"
exec bundle exec puma -C config/puma.rb
