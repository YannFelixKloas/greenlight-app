This app packages Greenlight <upstream>2.10.0.3</upstream>

Greenlight is the main front-end interface for a BigBlueButton server.

This app does **not** bundle BigBlueButton itself, but requires a backend server to be installed.
It comes pre-configured with a demo backend server, to test out the frontend.
