This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin@server.local<br/>
**Password**: changeme<br/>

Please change the admin password immediately!

## Setup BigBlueButton Endpoint

This Greenlight setup needs a BigBlueButton endpoint.

Open the File Manager of this App and edit the `.env` file.

Edit these two lines to add the endpoint and the secret:

```env
BIGBLUEBUTTON_ENDPOINT=
BIGBLUEBUTTON_SECRET=
```
